#!/bin/sh

# This bash script will convert hdf5 objects to text files using estpost.entropy
# Vivaswat Shastry -- Sep 2019

# If the appropriate K value was 4:
h5file="outfile/alfalfagl4"

# getting genotype estimates
./estpost.entropy -p gprob -s 0 "{$h5file}chain1.hdf5" "{$h5file}chain2.hdf5" -o genoest.txt

# getting ancestry (q) estimates
./estpost.entropy -p q -s 0 "{$h5file}chain1.hdf5" "{$h5file}chain2.hdf5" -o admixest.txt

# getting WAIC values
./estpost.entropy -p likdata -s 3 "{$h5file}chain1.hdf5"

