#!/bin/sh

module load gcc
# running for fst1/2 values
sed -i 's/fst<-c(0.2, 0.4)/fst<-c(0.05, 0.1)/' simgeno.R
Rscript simgeno.R
Rscript simcoverage.R

# placing fst1/2 files into genodata 
if [ ! -d "genodata" ]; then
	mkdir genodata
fi
mv *fst* genodata/

# changing to fst3/4 values for new simulated data
sed -i 's/fst<-c(0.05, 0.1)/fst<-c(0.2, 0.4)/' simgeno.R
Rscript simgeno.R
Rscript simcoverage.R

# renaming to match fst3/4 file names
mv	gl1.fst1.4x.mpgl	gl1.fst3.4x.mpgl  
mv	gl2.fst2.1x.mpgl	gl2.fst4.1x.mpgl  
mv	gl3.fst2.4x.mpgl	gl3.fst4.4x.mpgl  
mv	gl1.fst1.6x.mpgl	gl1.fst3.6x.mpgl  
mv	gl2.fst2.2x.mpgl	gl2.fst4.2x.mpgl  
mv	gl3.fst2.6x.mpgl	gl3.fst4.6x.mpgl  
mv	g.k1.fst1.txt		g.k1.fst3.txt     
mv	gl1.fst2.1x.mpgl	gl1.fst4.1x.mpgl  
mv	gl2.fst2.4x.mpgl	gl2.fst4.4x.mpgl  
mv	k1.fst1.txt			k1.fst3.txt       
mv	g.k1.fst2.txt		g.k1.fst4.txt     
mv	gl1.fst2.2x.mpgl	gl1.fst4.2x.mpgl  
mv	gl2.fst2.6x.mpgl	gl2.fst4.6x.mpgl  
mv	k1.fst2.txt			k1.fst4.txt       
mv	g.k2.fst1.txt		g.k2.fst3.txt     
mv	gl1.fst2.4x.mpgl	gl1.fst4.4x.mpgl  
mv	gl3.fst1.1x.mpgl	gl3.fst3.1x.mpgl  
mv	k2.fst1.txt			k2.fst3.txt       
mv	g.k2.fst2.txt		g.k2.fst4.txt     
mv	gl1.fst2.6x.mpgl	gl1.fst4.6x.mpgl  
mv	gl3.fst1.2x.mpgl	gl3.fst3.2x.mpgl  
mv	k2.fst2.txt			k2.fst4.txt       
mv	g.k3.fst1.txt		g.k3.fst3.txt     
mv	gl2.fst1.1x.mpgl	gl2.fst3.1x.mpgl  
mv	gl3.fst1.4x.mpgl	gl3.fst3.4x.mpgl  
mv	k3.fst1.txt			k3.fst3.txt       
mv	g.k3.fst2.txt		g.k3.fst4.txt     
mv	gl2.fst1.2x.mpgl	gl2.fst3.2x.mpgl  
mv	gl3.fst1.6x.mpgl	gl3.fst3.6x.mpgl  
mv	k3.fst2.txt	k3.fst4.txt
mv	gl1.fst1.1x.mpgl	gl1.fst3.1x.mpgl  
mv	gl2.fst1.4x.mpgl	gl2.fst3.4x.mpgl  
mv	gl3.fst2.1x.mpgl	gl3.fst4.1x.mpgl  
mv	gl1.fst1.2x.mpgl	gl1.fst3.2x.mpgl  
mv	gl2.fst1.6x.mpgl	gl2.fst3.6x.mpgl  
mv	gl3.fst2.2x.mpgl	gl3.fst4.2x.mpgl
mv gl3.fst1.12x.mpgl gl3.fst3.12x.mpgl
mv  gl3.fst2.12x.mpgl gl3.fst4.12x.mpgl
mv gl1.fst1.12x.mpgl gl1.fst3.12x.mpgl
mv gl1.fst2.12x.mpgl gl1.fst4.12x.mpgl
mv gl2.fst1.12x.mpgl gl2.fst3.12x.mpgl
mv gl2.fst2.12x.mpgl gl2.fst4.12x.mpgl


# moving all simulated genotypes from fst1/2/3/4 into current wd
mv genodata/* ./
