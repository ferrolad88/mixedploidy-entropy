#!/bin/sh
module load gcc r 
cd /project/evolgen/entropy_vivaswat/mixedploidy-entropy/

## for diploids
sed -i 's/hexaploid/diploid/g' qsimldakest.R

# uncomment the k=2 lines
sed -i '38,40 s/#//' qsimldakest.R
# comment the k=3 lines
sed -i '42,44 s/^/#/' qsimldakest.R
sed -i 's/g.k3.fst4.txt/g.k2.fst1.txt/' qsimldakest.R
sed -i 's/qk3fst4/qk2fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R
# comment the k=2 lines
sed -i '38,40 s/^/#/' qsimldakest.R
# uncomment the k=3 lines
sed -i '42,44 s/#//' qsimldakest.R
sed -i 's/g.k2.fst4.txt/g.k3.fst1.txt/' qsimldakest.R
sed -i 's/qk2fst4/qk3fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R

## for triploids
sed -i 's/diploid/triploid/g' qsimldakest.R

# uncomment the k=2 lines
sed -i '38,40 s/#//' qsimldakest.R
# comment the k=3 lines
sed -i '42,44 s/^/#/' qsimldakest.R
sed -i 's/g.k3.fst4.txt/g.k2.fst1.txt/' qsimldakest.R
sed -i 's/qk3fst4/qk2fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R
# comment the k=2 lines
sed -i '38,40 s/^/#/' qsimldakest.R
# uncomment the k=3 lines
sed -i '42,44 s/#//' qsimldakest.R
sed -i 's/g.k2.fst4.txt/g.k3.fst1.txt/' qsimldakest.R
sed -i 's/qk2fst4/qk3fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R

## for tetraploids
sed -i 's/triploid/tetraploid/g' qsimldakest.R

# uncomment the k=2 lines
sed -i '38,40 s/#//' qsimldakest.R
# comment the k=3 lines
sed -i '42,44 s/^/#/' qsimldakest.R
sed -i 's/g.k3.fst4.txt/g.k2.fst1.txt/' qsimldakest.R
sed -i 's/qk3fst4/qk2fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R
# comment the k=2 lines
sed -i '38,40 s/^/#/' qsimldakest.R
# uncomment the k=3 lines
sed -i '42,44 s/#//' qsimldakest.R
sed -i 's/g.k2.fst4.txt/g.k3.fst1.txt/' qsimldakest.R
sed -i 's/qk2fst4/qk3fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R

## for hexaploids
sed -i 's/tetraploid/hexaploid/g' qsimldakest.R

# uncomment the k=2 lines
sed -i '38,40 s/#//' qsimldakest.R
# comment the k=3 lines
sed -i '42,44 s/^/#/' qsimldakest.R
sed -i 's/g.k3.fst4.txt/g.k2.fst1.txt/' qsimldakest.R
sed -i 's/qk3fst4/qk2fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R
# comment the k=2 lines
sed -i '38,40 s/^/#/' qsimldakest.R
# uncomment the k=3 lines
sed -i '42,44 s/#//' qsimldakest.R
sed -i 's/g.k2.fst4.txt/g.k3.fst1.txt/' qsimldakest.R
sed -i 's/qk2fst4/qk3fst1/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst1/fst2/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst2/fst3/g' qsimldakest.R
Rscript qsimldakest.R
sed -i 's/fst3/fst4/g' qsimldakest.R
Rscript qsimldakest.R
