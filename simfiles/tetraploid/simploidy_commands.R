### reworking the sim_commands.R script for polyploidy
### Vivaswat Shastry, Jan 2019

set.seed(10041996)

fst<-c(0.05, 0.25)
nind<-100
nloci<-10000
nloci2<-2000
ploidy<-4

### --- simulate ancestral allele frequencies
anc.pi<-rbeta(nloci, 0.8, 0.8)

unlink("anc.pi.txt")
# writing the ancestral allele frequencies into the file 
write(anc.pi, file="anc.pi.txt")

### --- simulate cluster allele frequencies
# k signifies the cluster ID, since we know there are two pops
p.k1.fst1<-data.frame(pop1=numeric(nloci))
p.k2.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci))
p.k3.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci), pop3=numeric(nloci))

for(i in 1:nloci){
	p.k1.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k2.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k2.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k3.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,3]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
}

### --- simulate cluster allele frequencies
p.k1.fst2<-data.frame(pop1=numeric(nloci))
p.k2.fst2<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci))
p.k3.fst2<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci), pop3=numeric(nloci))

for(i in 1:nloci){
	p.k1.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))

	p.k2.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k2.fst2[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))

	p.k3.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k3.fst2[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k3.fst2[i,3]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
}

### --- simulate ancestry combinations (Q matrix lower triangle plus
### diagonal) for sampled individuals, choose from different hybrid
### classes.  Want to model different ancestry classes, not just
### admixture coefficients

### these will parameters for a multinomial draw for locus specific ancestries
Q.k1 <- rep(1, nind)
Q.k2 <- rbind(
			  matrix(c(1,0,0), ncol=3, nrow=0.25*nind, byrow=T),  ## 50 parentals type 1
			  matrix(c(0,0,1), ncol=3, nrow=0.25*nind, byrow=T),  ## 50 parentals type 2
			  matrix(c(0,1,0), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 F1s
			  matrix(c(0.25,0.5,0.25), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 F2s
			  matrix(c(0.5,0.5,0), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 1
			  matrix(c(0,0.5,0.5), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 2
			  matrix(c(0.375,0.25,0.375), ncol=3, nrow=0.1*nind, byrow=T)  ## 20 F3s
			  )
Q.k3 <- rbind(  ### NB: there are no three way hybrids
			  matrix(c(1,0,0,0,0,0), ncol=6, nrow=15, byrow=T),  ## 30 parentals type 1
			  matrix(c(0,0,1,0,0,0), ncol=6, nrow=15, byrow=T),  ## 30 parentals type 2
			  matrix(c(0,0,0,0,0,1), ncol=6, nrow=15, byrow=T),  ## 30 parentals type 3
			  matrix(c(0,1,0,0,0,0), ncol=6, nrow=5, byrow=T),  ## 10 F1s of 1/2
			  matrix(c(0,0,0,1,0,0), ncol=6, nrow=5, byrow=T),  ## 10 F1s of 1/3
			  matrix(c(0,0,0,0,1,0), ncol=6, nrow=5, byrow=T),  ## 10 F1s of 2/3
			  matrix(c(0.25,0.5,0.25,0,0,0), ncol=6, nrow=5, byrow=T),  ## 10 F2s between type 1 and 2
			  matrix(c(0.25,0,0,0.5,0,0.25), ncol=6, nrow=5, byrow=T),  ## 10 F2s between type 1 and 3
			  matrix(c(0,0,0.25,0,0.5,0.25), ncol=6, nrow=5, byrow=T),  ## 10 F2s between type 2 and 3
			  matrix(c(0.5,0.5,0,0,0,0), ncol=6, nrow=5, byrow=T),  ## 10 BC1s between 1/2 F1 and type 1
			  matrix(c(0.5,0,0,0.5,0,0), ncol=6, nrow=5, byrow=T),  ## 10 BC1s between 1/3 F1 and type 1
			  matrix(c(0,0,0.5,0,0.5,0), ncol=6, nrow=5, byrow=T),  ## 10 BC1s between 2/3 F1 and type 2
			  matrix(c(0.375,0.25,0.375,0,0,0), ncol=6, nrow=5, byrow=T),  ## 10 F3s of 1/2
			  matrix(c(0,0,0,0.375,0.25,0.375), ncol=6, nrow=5, byrow=T)   ## 10 F3s of 2/3
			  )

unlink("Q*")
write.table(Q.k1, file="Q.k1.txt", col.names=F, row.names=F)
write.table(Q.k2, file="Q.k2.txt", col.names=F, row.names=F)
write.table(Q.k3, file="Q.k3.txt", col.names=F, row.names=F)

### sample locus specific ancestries for all individuals from a
### multinomial with Q parameters ... translate these into allele
### copies so we can do Bernoulli trials to build up genotypes.  Store
### only genotypes for now

unlink("g*")

## k1
g.k1.fst1<-matrix(0, nrow=nind, ncol=nloci)
for ( locus in 1:nloci ){
	g.k1.fst1[,locus]<-rbinom(nind, ploidy, p.k1.fst1$pop1[locus])
}
g.k1.fst2<-matrix(0, nrow=nind, ncol=nloci)
for ( locus in 1:nloci ){
	g.k1.fst2[,locus]<-rbinom(nind, ploidy, p.k1.fst2$pop1[locus])
}
## printing genotype values into a text file 
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k1.fst2[,locus], file="g.k1.fst2.txt", append=T,
				col.names=F, row.names=F)
}
## k2
z<-numeric(nloci)
g.k2.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k2)){
	# basically, z contains a vector that is one-hot
	z<-rmultinom(nloci, 1, Q.k2[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		# retreive index of the 1, can be between 1 and 3
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){ # then it is receiving all its alleles from pop1
			g.k2.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst1$pop1[locus]) 
		}
		else if (zz == 2){ # then it is a combination of the two pops 
			# and we assume that we get two alleles from each pop
			g.k2.fst1[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k2.fst1$pop1[locus]) + rbinom(1, 0.5*ploidy, prob=p.k2.fst1$pop2[locus])
		}
		else{ # it has to be entirely from pop2
			g.k2.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst1$pop2[locus])
		}
	}
}

g.k2.fst2<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k2)){
	z<-rmultinom(nloci, 1, Q.k2[ind,])
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)
		if(zz == 1){
			g.k2.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst2$pop1[locus]) 
		}
		else if (zz == 2){
			g.k2.fst2[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k2.fst2$pop1[locus]) + rbinom(1, 0.5*ploidy, prob=p.k2.fst2$pop2[locus])
		}
		else{
			g.k2.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst2$pop2[locus]) 
		}
	}
}

# printing genotype values into a text file 
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k2.fst2[,locus], file="g.k2.fst2.txt", append=T,
				col.names=F, row.names=F)
}

#k3
g.k3.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k3)){
	z<-rmultinom(nloci, 1, Q.k3[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop1[locus]) 
		}
		else if (zz == 2){
			g.k3.fst1[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop2[locus])
		}
		else if (zz == 3){
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop2[locus]) 
		}
		else if (zz == 4){
			g.k3.fst1[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop3[locus])
		}
		else if (zz == 5){
			g.k3.fst1[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop2[locus]) + rbinom(1, 0.5*ploidy, prob=p.k3.fst1$pop3[locus])
		}
		else {
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop3[locus])
		}
	}
}

g.k3.fst2<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k3)){
	z<-rmultinom(nloci, 1, Q.k3[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop1[locus]) 
		}
		else if (zz == 2){
			g.k3.fst2[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop1[locus]) +
			rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop2[locus])
		}
		else if (zz == 3){
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop2[locus]) 
		}
		else if (zz == 4){
			g.k3.fst2[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop1[locus]) +
			rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop3[locus])
		}
		else if (zz == 5){
			g.k3.fst2[ind, locus] <- rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop2[locus]) +
			rbinom(1, 0.5*ploidy, prob=p.k3.fst2$pop3[locus])
		}
		else {
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop3[locus])
		}
	}
}

# printing genotype values into a text file 
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k3.fst2[,locus], file="g.k3.fst2.txt", append=T,
				col.names=F, row.names=F)
}



### write input files for entropy ... do not model variance in coverage for now
### 
### a) SNP counts    ... model 20x coverage to begin
### b) genotype likelihoods  (make sure we get the same as for SNP counts)

make.snpcount<-function(x){
	# matrix of size number of inds by number of alleles, we choose 2 for biallelic markers 
	y<-matrix(0, nrow=length(x), 2)
	for(i in 1:length(x)){
		if(x[i]==4){
			y[i,]<-c(20,0)
		}
		if (x[i]==3){
			y[i,]<-c(15,5)
		}
		else if (x[i]==2){
			y[i,]<-c(10,10)
		}
		else if (x[i]==1){
			y[i,]<-c(5,15)
		}
		else{
			y[i,]<-c(0,20)
		}
	}
	return(y)
}

make.gl<-function(x){
	y<-matrix(0, nrow=length(x), ploidy+1)
	for(i in 1:length(x)){
		if(x[i]==4){
			y[i,]<-c(208, 50, 25, 12, 0)
		}
		else if (x[i]==3){
			y[i,]<-c(230, 25, 10, 0, 60)
		}
		else if (x[i]==2){
			y[i,]<-c(120,10,0,10,120)
		}
		else if (x[i]==1){
			y[i,]<-c(60,0,10,25,230)
		}
		else if (x[i]==0){
			y[i,]<-c(0,12,25,50,208)
		}
		else {
			y[i,]<-c(0,0,0,0,0)
		}
	}
	return(y)
}


# remove the existing files so that it doesn't append
unlink("k*")
unlink("gl*")

### drop loci that are invariant
todrop<-apply(g.k1.fst1==0 | g.k1.fst1==2*nind, 2, sum)>0
g.k1.fst1<-g.k1.fst1[,!todrop]
g.k1.fst1<-g.k1.fst1[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k1.fst1.txt", append=T)
	write.table(make.snpcount(g.k1.fst1[,locus]), file="k1.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2, "\ndiscarded ind line"), file="gl1.fst1.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl1.fst1.mpgl", append=T)
	write.table(make.gl(g.k1.fst1[,locus]), file="gl1.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}

todrop<-apply(g.k1.fst2==0 | g.k1.fst2==2*nind, 2, sum)>0
g.k1.fst2<-g.k1.fst2[,!todrop]
g.k1.fst2<-g.k1.fst2[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k1.fst2.txt", append=T)
	write.table(make.snpcount(g.k1.fst2[,locus]), file="k1.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2, "\ndiscarded ind line"), file="gl1.fst2.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl1.fst2.mpgl", append=T)
	write.table(make.gl(g.k1.fst2[,locus]), file="gl1.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k2.fst1==0 | g.k2.fst1==2*nind, 2, sum)>0
g.k2.fst1<-g.k2.fst1[,!todrop]
g.k2.fst1<-g.k2.fst1[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k2.fst1.txt", append=T)
	write.table(make.snpcount(g.k2.fst1[,locus]), file="k2.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2), "\ndiscarded ind line"), file="gl2.fst1.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl2.fst1.mpgl", append=T)
	write.table(make.gl(g.k2.fst1[,locus]), file="gl2.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k2.fst2==0 | g.k2.fst2==2*nind, 2, sum)>0
g.k2.fst2<-g.k2.fst2[,!todrop]
g.k2.fst2<-g.k2.fst2[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k2.fst2.txt", append=T)
	write.table(make.snpcount(g.k2.fst2[,locus]), file="k2.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2), "\ndiscarded ind line"), file="gl2.fst2.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl2.fst2.mpgl", append=T)
	write.table(make.gl(g.k2.fst2[,locus]), file="gl2.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k3.fst1==0 | g.k3.fst1==2*nind, 2, sum)>0
g.k3.fst1<-g.k3.fst1[,!todrop]
g.k3.fst1<-g.k3.fst1[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k3.fst1.txt", append=T)
	write.table(make.snpcount(g.k3.fst1[,locus]), file="k3.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2, "\ndiscarded ind line"), file="gl3.fst1.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl3.fst1.mpgl", append=T)
	write.table(make.gl(g.k3.fst1[,locus]), file="gl3.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}

todrop<-apply(g.k3.fst2==0 | g.k3.fst2==2*nind, 2, sum)>0
g.k3.fst2<-g.k3.fst2[,!todrop]
g.k3.fst2<-g.k3.fst2[,1:nloci2]
for(locus in 1:nloci2){
	cat(paste("locus", locus, "\n"), file="k3.fst2.txt", append=T)
	write.table(make.snpcount(g.k3.fst2[,locus]), file="k3.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, nloci2, "\ndiscarded ind line"), file="gl3.fst2.mpgl")
for(locus in 1:nloci2){
	cat(paste0("\nlocus", locus, " "), file="gl3.fst2.mpgl", append=T)
	write.table(make.gl(g.k3.fst2[,locus]), file="gl3.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}

q.k2 <- rbind(
			  matrix(c(0.9,0.1), ncol=2, nrow=0.25*nind, byrow=T),  ## 50 parentals type 1
			  matrix(c(0.1,0.9), ncol=2, nrow=0.25*nind, byrow=T),  ## 50 parentals type 2
			  matrix(c(0.6,0.4), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 F1s
			  matrix(c(0.4,0.6), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 F2s
			  matrix(c(0.7,0.3), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 1
			  matrix(c(0.3,0.7), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 2
			  matrix(c(0.6,0.4), ncol=2, nrow=0.1*nind, byrow=T)  ## 20 F3s
			  )
write.table(q.k2,file="qk2.txt",col.names=F,row.names=F)

q.k3 <- rbind(  ### NB: there are no three way hybrids
			  matrix(c(0.9,0.05,0.05), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 1
			  matrix(c(0.05,0.9,0.05), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 2
			  matrix(c(0.05,0.05,0.9), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 3
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 1/2
			  matrix(c(0.45,0.1,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 1/3
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 2/3
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 1 and 2
			  matrix(c(0.45,0.1,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 1 and 3
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 2 and 3
			  matrix(c(0.7,0.25,0.05), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 1/2 F1 and type 1
			  matrix(c(0.7,0.05,0.25), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 1/3 F1 and type 1
			  matrix(c(0.05,0.7,0.25), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 2/3 F1 and type 2
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F3s of 1/2
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T)   ## 10 F3s of 2/3
			  )
write.table(q.k3,file="qk3.txt",col.names=F,row.names=F)

## How ancestry informative are the markers?
## plot(p.k2.fst2[,1], p.k2.fst2[,2])
## hist(abs(p.k2.fst2[,1] - p.k2.fst2[,2]))

### how informative are the markers about Fst?
## hist(abs(p.k1.fst1[,1] - anc.pi))

