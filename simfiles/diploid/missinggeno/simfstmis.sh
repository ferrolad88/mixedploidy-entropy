#!/bin/sh

module load gcc r
# running for fst1/2 values
sed -i 's/fst3/fst1/g' simmissing.R
Rscript simmissing.R

sed -i 's/fst1/fst2/g' simmissing.R
Rscript simmissing.R

sed -i 's/fst2/fst3/g' simmissing.R
Rscript simmissing.R
