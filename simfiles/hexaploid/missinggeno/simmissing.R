### Vivaswat Shastry, Jan 2019
### file for filling with missing data 

## assuming an fst of 0.1 and a coverage of 12x (not varying this)

## reading in the genotype files
g.k1<-matrix(scan("../g.k1.fst3.txt"), nrow=100, ncol=2000)
g.k2<-matrix(scan("../g.k2.fst3.txt"), nrow=100, ncol=2000)
g.k3<-matrix(scan("../g.k3.fst3.txt"), nrow=100, ncol=2000)

nloci2<-2000
nind<-100
pl<-6

missing<-c(0.1, 0.2, 0.3, 0.4)

make.snpcount<-function(x){
	y<-matrix(0, nrow=length(x), 2)
	for(i in 1:length(x)){
		if(x[i]==6){
			y[i,]<-c(12,0)
		}
		else if(x[i]==5){
			y[i,]<-c(10,2)
		}
		else if(x[i]==4){
			y[i,]<-c(8,4)
		}
		else if (x[i]==3){
			y[i,]<-c(6,6)
		}
		else if (x[i]==2){
			y[i,]<-c(4,8)
		}
		else if(x[i]==1){
			y[i,]<-c(2,10)
		}
		else if(x[i]==0){
			y[i,]<-c(0,12)
		}
		else{
			y[i,]<-c(0,0)
		}
	}
	return(y)
}

make.gl12<-function(x){
	y<-matrix(0, nrow=length(x), ncol=7)
	for(i in 1:length(x)){
		if(x[i]==6){
			y[i,]<-c(501, 93, 57, 36, 21, 10, 0)
		}
		else if(x[i]==5){
			cov12<-list(c(443, 70, 38, 20, 8, 0, 25), c(346, 42, 18, 6, 0, 0, 95))
			y[i,]<-ifelse(runif(1)<0.7, cov12[1], cov12[2])[[1]]
		}
		else if(x[i]==4){
			cov12<-list(c(346, 42, 18, 6, 0, 0, 95), c(301, 32, 12, 3, 0, 4, 134))
			y[i,]<-ifelse(runif(1)<0.4, cov12[1], cov12[2])[[1]]
		}
		else if (x[i]==3){
			y[i,]<-c(215, 15, 3, 0, 3, 15, 215)
		}
		else if (x[i]==2){
			cov12<-list(c(95, 0, 0, 6, 18, 42, 346), c(134, 4, 0, 3, 12, 32, 301))
			y[i,]<-ifelse(runif(1)<0.4, cov12[1], cov12[2])[[1]]
		}
		else if (x[i]==1){
			cov12<-list(c(25, 0, 8, 20, 38, 70, 443), c(95, 0, 0, 6, 18, 42, 346))
			y[i,]<-ifelse(runif(1)<0.7, cov12[1], cov12[2])[[1]]
		}
		else{
			y[i,]<-c(0, 10, 21, 36, 57, 93, 501)
		}
	}
	return(y)
}

ran<-matrix(0, nrow=length(missing), ncol=0.4*nind*nloci2)

for(i in 1:length(missing)){
	ran[i,1:(missing[i]*nind*nloci2)]<-sample(1:(nind*nloci2), missing[i]*nind*nloci2, replace=F)
}

for(i in 1:length(missing)){
	g.k1[ran[i,]]<--9
	for(locus in 1:nloci2){
		cat(paste("locus", locus, "\n"), file=paste0("k1.mis",i,"fst3.txt"), append=T)
		write.table(make.snpcount(g.k1[,locus]), file=paste0("k1.mis",i,"fst3.txt"), append=T,
					col.names=F, row.names=F)
	}
	cat(paste(nind, nloci2, "\ndiscarded ind line"), file=paste0("gl1.mis",i,".fst3.mpgl"))
	for(locus in 1:nloci2){
		cat(paste0("\nlocus", locus, " "), file=paste0("gl1.mis",i,".fst3.mpgl"), append=T)
		write.table(make.gl12(g.k1[,locus]), file=paste0("gl1.mis",i,".fst3.mpgl"), append=T,
					col.names=F, row.names=F, eol=" ")
	}
}

for(i in 1:length(missing)){
	g.k2[ran[i,]]<--9
	for(locus in 1:nloci2){
		cat(paste("locus", locus, "\n"), file=paste0("k2.mis",i,"fst3.txt"), append=T)
		write.table(make.snpcount(g.k2[,locus]), file=paste0("k2.mis",i,"fst3.txt"), append=T,
					col.names=F, row.names=F)
	}
	cat(paste(nind, nloci2, "\ndiscarded ind line"), file=paste0("gl2.mis",i,".fst3.mpgl"))
	for(locus in 1:nloci2){
		cat(paste0("\nlocus", locus, " "), file=paste0("gl2.mis",i,".fst3.mpgl"), append=T)
		write.table(make.gl12(g.k2[,locus]), file=paste0("gl2.mis",i,".fst3.mpgl"), append=T,
					col.names=F, row.names=F, eol=" ")
	}
}

for(i in 1:length(missing)){
	g.k3[ran[i,]]<--9
	for(locus in 1:nloci2){
		cat(paste("locus", locus, "\n"), file=paste0("k3.mis",i,"fst3.txt"), append=T)
		write.table(make.snpcount(g.k3[,locus]), file=paste0("k3.mis",i,"fst3.txt"), append=T,
					col.names=F, row.names=F)
	}
	cat(paste(nind, nloci2, "\ndiscarded ind line"), file=paste0("gl3.mis",i,".fst3.mpgl"))
	for(locus in 1:nloci2){
		cat(paste0("\nlocus", locus, " "), file=paste0("gl3.mis",i,".fst3.mpgl"), append=T)
		write.table(make.gl12(g.k3[,locus]), file=paste0("gl3.mis",i,".fst3.mpgl"), append=T,
					col.names=F, row.names=F, eol=" ")
	}
}

## printing out the genotypes are labeled as missing
for(i in 1:length(missing)){
	write.table(t(ran[i,1:(missing[i]*nind*nloci2)]), file=paste0("ranidxmis",i,"fst3.txt"), col.names=F, row.names=F)
}
