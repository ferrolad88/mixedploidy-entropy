### reworking the sim_commands.R script for polyploidy
### generate mixed ploidy individuals with corresponding genotypes
### Vivaswat Shastry, Dec 2018

set.seed(10041996)

fst<-0.25
nind<-100
nloci<-10000
nloci2<-2000

## sample pl[ind] levels
#unlink("plind.txt")
#pl<-c(rep(4,0.5*nind),rep(3,0.25*nind),rep(2,0.25*nind))
#pl<-c(rep(6,nind))
#pl<-c(rep(3,0.5*nind),rep(2,0.5*nind))
#pl<-rep(2,nind)
#write(pl,file="plind.txt",ncol=1)

### --- simulate ancestral allele frequencies
anc.pi<-rbeta(nloci, 0.4, 0.75)

unlink("anc.pi.txt")
# writing the ancestral allele frequencies into the file 
write(anc.pi, file="anc.pi.txt")

### --- simulate cluster allele frequencies
# k signifies the cluster ID, since we know there are two pops
p.k1.fst1<-data.frame(pop1=numeric(nloci))
p.k2.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci))
p.k3.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci), pop3=numeric(nloci))

for(i in 1:nloci){
	p.k1.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k2.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k2.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k3.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,3]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
}
### --- simulate ancestry combinations (Q matrix lower triangle plus
### diagonal) for sampled individuals, choose from different hybrid
### classes.  Want to model different ancestry classes, not just
### admixture coefficients

### these will parameters for a multinomial draw for locus specific ancestries
Q.k1 <- rep(1, nind)
Q.k2 <- rbind(
			  matrix(c(1,0,0), ncol=3, nrow=0.25*nind, byrow=T),  ## 50 parentals type 1
			  matrix(c(0,0,1), ncol=3, nrow=0.25*nind, byrow=T),  ## 50 parentals type 2
			  matrix(c(0,1,0), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 F1s
			  matrix(c(0.25,0.5,0.25), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 F2s
			  matrix(c(0.5,0.5,0), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 1
			  matrix(c(0,0.5,0.5), ncol=3, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 2
			  matrix(c(0.375,0.25,0.375), ncol=3, nrow=0.1*nind, byrow=T)  ## 20 F3s
			  )
Q.k3 <- rbind(  ### NB: there are no three way hybrids
			  matrix(c(1,0,0,0,0,0), ncol=6, nrow=30, byrow=T),  ## 30 parentals type 1
			  matrix(c(0,0,1,0,0,0), ncol=6, nrow=30, byrow=T),  ## 30 parentals type 2
			  matrix(c(0,0,0,0,0,1), ncol=6, nrow=30, byrow=T),  ## 30 parentals type 3
			  matrix(c(0,1,0,0,0,0), ncol=6, nrow=10, byrow=T),  ## 10 F1s of 1/2
			  matrix(c(0,0,0,1,0,0), ncol=6, nrow=10, byrow=T),  ## 10 F1s of 1/3
			  matrix(c(0,0,0,0,1,0), ncol=6, nrow=10, byrow=T),  ## 10 F1s of 2/3
			  matrix(c(0.25,0.5,0.25,0,0,0), ncol=6, nrow=10, byrow=T),  ## 10 F2s between type 1 and 2
			  matrix(c(0.25,0,0,0.5,0,0.25), ncol=6, nrow=10, byrow=T),  ## 10 F2s between type 1 and 3
			  matrix(c(0,0,0.25,0,0.5,0.25), ncol=6, nrow=10, byrow=T),  ## 10 F2s between type 2 and 3
			  matrix(c(0.5,0.5,0,0,0,0), ncol=6, nrow=10, byrow=T),  ## 10 BC1s between 1/2 F1 and type 1
			  matrix(c(0.5,0,0,0.5,0,0), ncol=6, nrow=10, byrow=T),  ## 10 BC1s between 1/3 F1 and type 1
			  matrix(c(0,0,0.5,0,0.5,0), ncol=6, nrow=10, byrow=T),  ## 10 BC1s between 2/3 F1 and type 2
			  matrix(c(0.375,0.25,0.375,0,0,0), ncol=6, nrow=10, byrow=T),  ## 10 F3s of 1/2
			  matrix(c(0,0,0,0.375,0.25,0.375), ncol=6, nrow=10, byrow=T)   ## 10 F3s of 2/3
			  )

unlink("Q*")
write.table(Q.k1, file="Q.k1.txt", col.names=F, row.names=F)
write.table(Q.k2, file="Q.k2.txt", col.names=F, row.names=F)
write.table(Q.k3, file="Q.k3.txt", col.names=F, row.names=F)

### sample locus specific ancestries for all individuals from a
### multinomial with Q parameters ... translate these into allele
### copies so we can do Bernoulli trials to build up genotypes.  Store
### only genotypes for now

unlink("g*")

## k1
g.k1.fst1<-matrix(0, nrow=nind, ncol=nloci)
for ( locus in 1:nloci ){
	for(ind in 1:nind){
		g.k1.fst1[ind,locus]<-rbinom(1, pl[ind], p.k1.fst1$pop1[locus])
	}
}
# printing genotype values into a text file 
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k1.fst1[,locus], file="g.k1.txt", append=T,
				col.names=F, row.names=F)
}

## k2
z<-numeric(nloci)
g.k2.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k2)){
	# basically, z contains a vector that is one-hot
	z<-rmultinom(nloci, 1, Q.k2[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		# retreive index of the 1, can be between 1 and 3
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(pl[ind]==3){
			if(zz == 1){ # then it is receiving all its alleles from pop1
				g.k2.fst1[ind, locus] <- rbinom(1, 3, prob=p.k2.fst1$pop1[locus]) 
			}
			else if (zz == 2){ # then it is a combination of the two pops 
				u <- rbinom(1, 1, 0.5)
				g.k2.fst1[ind, locus] <- u*(rbinom(1, 1, prob=p.k2.fst1$pop1[locus]) + rbinom(1, 2, prob=p.k2.fst1$pop2[locus])) + (1-u)*(rbinom(1, 2, prob=p.k2.fst1$pop1[locus]) + rbinom(1, 1, prob=p.k2.fst1$pop2[locus]))
			}
			else{ # it has to be entirely from pop2
				g.k2.fst1[ind, locus] <- rbinom(1, 3, prob=p.k2.fst1$pop2[locus])
			}
		}
		else {
			if(zz == 1){ # then it is receiving all its alleles from pop1
				g.k2.fst1[ind, locus] <- rbinom(1, pl[ind], prob=p.k2.fst1$pop1[locus]) 
			}
			else if (zz == 2){ # then it is a combination of the two pops 
				# and we assume that we get two alleles from each pop
				g.k2.fst1[ind, locus] <- rbinom(1, 0.5*pl[ind], prob=p.k2.fst1$pop1[locus]) + rbinom(1, 0.5*pl[ind], prob=p.k2.fst1$pop2[locus])
			}
			else{ # it has to be entirely from pop2
				g.k2.fst1[ind, locus] <- rbinom(1, pl[ind], prob=p.k2.fst1$pop2[locus])
			}
		}
	}
}
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k2.fst1[,locus], file="g.k2.txt", append=T,
				col.names=F, row.names=F)
}

#k3
g.k3.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k3)){
	z<-rmultinom(nloci, 1, Q.k3[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(pl[ind]==3){
			if(zz == 1){
				g.k3.fst1[ind, locus] <- rbinom(1, 3, prob=p.k3.fst1$pop1[locus]) 
			}
			else if (zz == 2){
				u <- rbinom(1, 1, 0.5)
				g.k3.fst1[ind, locus] <- u*(rbinom(1, 1, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 2, prob=p.k3.fst1$pop2[locus])) + (1-u)*(rbinom(1, 2, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 1, prob=p.k3.fst1$pop2[locus]))
			}
			else if (zz == 3){
				g.k3.fst1[ind, locus] <- rbinom(1, 3, prob=p.k3.fst1$pop2[locus]) 
			}
			else if (zz == 4){
				u <- rbinom(1, 1, 0.5)
				g.k3.fst1[ind, locus] <- u*(rbinom(1, 1, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 2, prob=p.k3.fst1$pop3[locus])) + (1-u)*(rbinom(1, 2, prob=p.k3.fst1$pop1[locus]) + rbinom(1, 1, prob=p.k3.fst1$pop3[locus]))
			}
			else if (zz == 5){
				u <- rbinom(1, 1, 0.5)
				g.k3.fst1[ind, locus] <- u*(rbinom(1, 1, prob=p.k3.fst1$pop2[locus]) + rbinom(1, 2, prob=p.k3.fst1$pop3[locus])) + (1-u)*(rbinom(1, 2, prob=p.k3.fst1$pop2[locus]) + rbinom(1, 1, prob=p.k3.fst1$pop3[locus]))
			}
			else {
				g.k3.fst1[ind, locus] <- rbinom(1, 3, prob=p.k3.fst1$pop3[locus])
			}
		}
		else{
			if(zz == 1){
				g.k3.fst1[ind, locus] <- rbinom(1, pl[ind], prob=p.k3.fst1$pop1[locus]) 
			}
			else if (zz == 2){
				g.k3.fst1[ind, locus] <- rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop1[locus]) + rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop2[locus])
			}
			else if (zz == 3){
				g.k3.fst1[ind, locus] <- rbinom(1, pl[ind], prob=p.k3.fst1$pop2[locus]) 
			}
			else if (zz == 4){
				g.k3.fst1[ind, locus] <- rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop1[locus]) + rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop3[locus])
			}
			else if (zz == 5){
				g.k3.fst1[ind, locus] <- rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop2[locus]) + rbinom(1, 0.5*pl[ind], prob=p.k3.fst1$pop3[locus])
			}
			else {
				g.k3.fst1[ind, locus] <- rbinom(1, pl[ind], prob=p.k3.fst1$pop3[locus])
			}
		}
	}
}
# printing genotype values into a text file 
for(locus in 1:nloci){
	#cat(paste("locus", locus, "\n"), file="g.k2.fst2.txt", append=T)
	write.table(g.k3.fst1[,locus], file="g.k3.txt", append=T,
				col.names=F, row.names=F)
}



### write input files for entropy ... do not model variance in coverage for now
### 
### a) SNP counts    ... model 20x coverage to begin
### b) genotype likelihoods  (make sure we get the same as for SNP counts)

make.snpcount<-function(x){
	# matrix of size number of inds by number of alleles, we choose 2 for biallelic markers 
	y<-matrix(0, nrow=length(x), 2)
	for(i in 1:length(x)){
		if(pl[i]==6){
			if(x[i]==6){
				y[i,]<-c(24,0)
			}
			else if(x[i]==5){
				y[i,]<-c(20,4)
			}
			else if(x[i]==4){
				y[i,]<-c(16,8)
			}
			else if (x[i]==3){
				y[i,]<-c(12,12)
			}
			else if (x[i]==2){
				y[i,]<-c(8,16)
			}
			else if (x[i]==1){
				y[i,]<-c(4,20)
			}
			else{
				y[i,]<-c(0,24)
			}
		}
	}
	return(y)
}

make.gl<-function(x){
	Y<-NULL
	for(i in 1:length(x)){
		y<-vector(length=(pl[i]+1))
		## these numbers are based on simulations with GATK
		# 0 ref, 12 alt (symmetry holds)
		# 3 ref, 9 alt (symmetry holds)
		# 6 ref, 6 alt
		if(pl[i]==6){
			if(x[i]==6){
				y<-c(250, 50, 35, 25, 15, 7, 0)
			}
			else if(x[i]==5){
				y<-c(230, 24, 18, 12, 6, 0, 20)
			}
			else if(x[i]==4){
				y<-c(220, 18, 12, 5, 0, 6, 180)
			}
			else if (x[i]==3){
				y<-c(215, 15, 3, 0, 3, 15, 215)
			}
			else if (x[i]==2){
				y<-c(180, 6, 0, 5, 12, 18, 220)
			}
			else if (x[i]==1){
				y<-c(20, 0, 6, 12, 18, 24, 230)
			}
			else{
				y<-c(0, 7, 15, 25, 35, 50, 250)
			}
		}
		Y<-append(Y, y)
	}
	return(Y)
}

# remove the existing files so that it doesn't append
unlink("k*")
unlink("gl*")

### drop loci that are invariant
todrop<-apply(g.k1.fst1==0 | g.k1.fst1==2*nind, 2, sum)>0
g.k1.fst1<-g.k1.fst1[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k1.fst1.txt", append=T)
	write.table(make.snpcount(g.k1.fst1[,locus]), file="k1.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl1.fst1.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl1.fst1.mpgl", append=T)
	write.table(make.gl(g.k1.fst1[,locus]), file="gl1.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}

todrop<-apply(g.k1.fst2==0 | g.k1.fst2==2*nind, 2, sum)>0
g.k1.fst2<-g.k1.fst2[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k1.fst2.txt", append=T)
	write.table(make.snpcount(g.k1.fst2[,locus]), file="k1.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl1.fst2.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl1.fst2.mpgl", append=T)
	write.table(make.gl(g.k1.fst2[,locus]), file="gl1.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k2.fst1==0 | g.k2.fst1==2*nind, 2, sum)>0
g.k2.fst1<-g.k2.fst1[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k2.fst1.txt", append=T)
	write.table(make.snpcount(g.k2.fst1[,locus]), file="k2.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl2.fst1.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl2.fst1.mpgl", append=T)
	write.table(make.gl(g.k2.fst1[,locus]), file="gl2.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k2.fst2==0 | g.k2.fst2==2*nind, 2, sum)>0
g.k2.fst2<-g.k2.fst2[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k2.fst2.txt", append=T)
	write.table(make.snpcount(g.k2.fst2[,locus]), file="k2.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl2.fst2.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl2.fst2.mpgl", append=T)
	write.table(make.gl(g.k2.fst2[,locus]), file="gl2.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


todrop<-apply(g.k3.fst1==0 | g.k3.fst1==2*nind, 2, sum)>0
g.k3.fst1<-g.k3.fst1[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k3.fst1.txt", append=T)
	write.table(make.snpcount(g.k3.fst1[,locus]), file="k3.fst1.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl3.fst1.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl3.fst1.mpgl", append=T)
	write.table(make.gl(g.k3.fst1[,locus]), file="gl3.fst1.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}

todrop<-apply(g.k3.fst2==0 | g.k3.fst2==2*nind, 2, sum)>0
g.k3.fst2<-g.k3.fst2[,!todrop]
for(locus in 1:(nloci-sum(todrop))){
	cat(paste("locus", locus, "\n"), file="k3.fst2.txt", append=T)
	write.table(make.snpcount(g.k3.fst2[,locus]), file="k3.fst2.txt", append=T,
				col.names=F, row.names=F)
}
cat(paste(nind, (nloci-sum(todrop)), "\ndiscarded ind line"), file="gl3.fst2.mpgl")
for(locus in 1:(nloci-sum(todrop))){
	cat(paste0("\nlocus", locus, " "), file="gl3.fst2.mpgl", append=T)
	write.table(make.gl(g.k3.fst2[,locus]), file="gl3.fst2.mpgl", append=T,
				col.names=F, row.names=F, eol=" ")
}


q.k2 <- rbind(
			  matrix(c(0.9,0.1), ncol=2, nrow=0.25*nind, byrow=T),  ## 50 parentals type 1
			  matrix(c(0.1,0.9), ncol=2, nrow=0.25*nind, byrow=T),  ## 50 parentals type 2
			  matrix(c(0.6,0.4), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 F1s
			  matrix(c(0.4,0.6), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 F2s
			  matrix(c(0.7,0.3), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 1
			  matrix(c(0.3,0.7), ncol=2, nrow=0.1*nind, byrow=T),  ## 20 BC1s to type 2
			  matrix(c(0.6,0.4), ncol=2, nrow=0.1*nind, byrow=T)  ## 20 F3s
			  )
write.table(q.k2,file="qk2.txt",col.names=F,row.names=F)

q.k3 <- rbind(  ### NB: there are no three way hybrids
			  matrix(c(0.9,0.05,0.05), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 1
			  matrix(c(0.05,0.9,0.05), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 2
			  matrix(c(0.05,0.05,0.9), ncol=3, nrow=30, byrow=T),  ## 30 parentals type 3
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 1/2
			  matrix(c(0.45,0.1,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 1/3
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F1s of 2/3
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 1 and 2
			  matrix(c(0.45,0.1,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 1 and 3
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T),  ## 10 F2s between type 2 and 3
			  matrix(c(0.7,0.25,0.05), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 1/2 F1 and type 1
			  matrix(c(0.7,0.05,0.25), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 1/3 F1 and type 1
			  matrix(c(0.05,0.7,0.25), ncol=3, nrow=10, byrow=T),  ## 10 BC1s between 2/3 F1 and type 2
			  matrix(c(0.45,0.45,0.1), ncol=3, nrow=10, byrow=T),  ## 10 F3s of 1/2
			  matrix(c(0.1,0.45,0.45), ncol=3, nrow=10, byrow=T)   ## 10 F3s of 2/3
			  )
write.table(q.k3,file="qk3.txt",col.names=F,row.names=F)

## How ancestry informative are the markers?
## plot(p.k2.fst2[,1], p.k2.fst2[,2])
## hist(abs(p.k2.fst2[,1] - p.k2.fst2[,2]))

### how informative are the markers about Fst?
## hist(abs(p.k1.fst1[,1] - anc.pi))

