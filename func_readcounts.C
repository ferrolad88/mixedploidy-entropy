#include <iostream>
#include <sstream>
#include <fstream>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sf_exp.h>
#include <float.h>
#include <algorithm>

#include <math.h>
#include "hdf5.h"

#include "entropy.h"

using namespace std;

// This file is deprecated since entropy no longer supports using read counts as a way
// of passing in likelihoods for the data 

/* gibbs update of genotypes */
// P(X|error, g)*P(g|z, p) (X is read data)
void updateGenotype(datadimcont * datadim, paramcont * params, datacont * data){
  int i, j;

  if(samePloidy){
    switch(datadim->pl){
    case 1:
      for(i=0; i<datadim->nloci; i++){
	for(j=0; j<datadim->nind; j++){
	  updateGenoHap(i, j, datadim, params, data, 1);
	}
      }
      break;
    case 2:
      for(i=0; i<datadim->nloci; i++){
	for(j=0; j<datadim->nind; j++){
	  updateGenoDip(i, j, datadim, params, data, 1);
	}
      }
      break;
    case 3:
      for(i=0; i<datadim->nloci; i++){
	for(j=0; j<datadim->nind; j++){
	  updateGenoTri(i, j, datadim, params, data, 1);
	}
      }
      break;
    case 4:
      for(i=0; i<datadim->nloci; i++){
	for(j=0; j<datadim->nind; j++){
	  updateGenoTetra(i, j, datadim, params, data, 1);
	}
      }
      break;
    case 6:
      for(i=0; i<datadim->nloci; i++){
	for(j=0; j<datadim->nind; j++){
	  updateGenoHexa(i, j, datadim, params, data, 1);
	}
      }
      break;
    }
  }
  else{
    for(i=0; i<datadim->nloci; i++){
      for(j=0; j<datadim->nind; j++){
	switch(gsl_matrix_int_get(params->ploidy, j, i)){
	case 1:
	  updateGenoHap(i, j, datadim, params, data, 1);
	  break;
	case 2:
	  updateGenoDip(i, j, datadim, params, data, 1);
	  break;
	case 3:
	  updateGenoTri(i, j, datadim, params, data, 1);
	  break;
	case 4:
	  updateGenoTetra(i, j, datadim, params, data, 1);
	  break;
	case 6:
	  updateGenoHexa(i, j, datadim, params, data, 1);
	  break;
	}
      }
    }
  }
} 

void likdata(datadimcont * datadim, paramcont * params, datacont * data,
	     auxcont * auxvar){

  int i, j, m;
  int dosage; 
  double sumlogprob = 0.0;
  double tmpprob = 0.0;

  // sum log P( X | G)
  for(i=0; i<datadim->nloci; i++){
    data->error = gsl_vector_get(data->errorvector, i);
    for(j=0; j<datadim->nind; j++){
      dosage = 0;
      for(m=0; m<gsl_matrix_int_get(params->ploidy, j, i); m++){
	dosage += gsl_matrix_int_get(params->g[i], m, j);
      }

      switch(gsl_matrix_int_get(params->ploidy, j, i)){
      case 1:
	if(dosage == 0){
	  tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						  data->error,
						  gsl_matrix_int_get(data->nreads, i, j)));
      }
      else{
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						1-data->error,
						gsl_matrix_int_get(data->nreads, i, j)));
      }
      gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      break;
    case 2:
      if(dosage == 0){
	// use fix0prob to protect against log(0), which can arise
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						data->error, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 1){ // heterozygotes
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						0.5, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else{ // other homozygote
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						1-data->error, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);

      }
      break;
    case 3:
      if(dosage == 0){
	// use fix0prob to protect against log(0), which can arise
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						data->error, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 1){ 
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(1+data->error)/3, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 2){ 
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(2-data->error)/3,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else{
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						1-data->error,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      break;
    case 4:
      if(dosage == 0){
	// use fix0prob to protect against log(0), which can arise
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						data->error, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 1){ 
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						0.25*(1+2*data->error), 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 2) {
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						0.5,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 3) {
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						0.25*(3-2*data->error),
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else {
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						1-data->error,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      break;
    case 6:
      if(dosage == 0){
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						data->error, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 1){ 
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(1+4*data->error)/6, 
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 2) {
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(2+2*data->error)/6,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 3) {
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						0.5,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 4){
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(4-2*data->error)/6,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else if(dosage == 5){
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(5-4*data->error)/6,
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      else{
	tmpprob = fix0prob(gsl_ran_binomial_pdf(gsl_matrix_int_get(data->allelecount, i, j),
						(1-data->error),
						gsl_matrix_int_get(data->nreads, i, j)));
	gsl_vector_set(params->likdata, i*datadim->nind+j, tmpprob);
      }
      break;       
    }
  }
}
// sum log P( G | Z, P)
for(i=0; i<datadim->nloci; i++){
  for(j=0; j<datadim->nind; j++){
    sumlogprob = log(gsl_vector_get(params->likdata, i*datadim->nind+j));      
    for(m=0; m<gsl_matrix_int_get(params->ploidy, j, i); m++){
      if(gsl_matrix_int_get(params->g[i], m, j) == 1){
	tmpprob = fix0prob( gsl_matrix_get(params->p, i, 
					   gsl_matrix_int_get(params->z[i], m, j)));
	sumlogprob += log(tmpprob); 
      }
      else{
	tmpprob = fix0prob(1-gsl_matrix_get(params->p, i, 
					    gsl_matrix_int_get(params->z[i], m, j)));
	sumlogprob += log(tmpprob);
      }
    }
    gsl_vector_set(params->likdata, i*datadim->nind+j, gsl_sf_exp(sumlogprob));
  }
 }
}

/* determine and record the number of individuals per locus, based on readsfile */
int getNind(string filename){
  string line;
  int lociCtr = -1;
  int indCtr = 0;
  ifstream infile;

  // open file
  infile.open(filename.c_str());
  if (!infile){
    cerr << "Cannot open file " << filename << endl;
    exit(1);
  }

  while (getline(infile, line)){ // read a line of input into line
    if (MAR == line[0]){ // MAR is the character designating a locus
      if (lociCtr > -1){
	break;
      }
      indCtr = 0;
      lociCtr++;
    }
    else { // this is a line with count data
      indCtr++;
    }
  }
  infile.close();
  return(indCtr);
}


/* determine the number of loci from reads file */
int getNloci(string filename){
  string line;
  int lociCtr = 0;
  ifstream infile;

  // open file
  infile.open(filename.c_str());
  if (!infile){
    cerr << "Cannot open file " << filename << endl;
    exit(1);
  }
  // get number of loci
  while (getline(infile, line)){ // read a line of input into line
    if ( MAR == line[0]){ // MAR is the character designating a locus
      lociCtr++;
    }
  }
  infile.close();
  return(lociCtr);
}

/* determine and record the number of alleles per locus */
int getNallele(string filename, gsl_vector_int * nallele){
  string line, oneword;
  int lociCtr = -1;
  int wordCtr = 0;
  int max = 0;
  int first = 1;
  ifstream infile;

  // open file
  infile.open(filename.c_str());
  if (!infile){
    cerr << "Cannot open file " << filename << endl;
    exit(1);
  }

  // determine number of alleles per locus
  while (getline(infile, line)){ // read a line of input into line
    if (MAR == line[0]){ // MAR is the character designating a locus
      lociCtr++;
      first = 1;

    }
    else if (first == 1){ // this is a line with count data
      first = 0;
      wordCtr = 0;
      istringstream stream(line);
      while (stream >> oneword){ // read a word at a time
	wordCtr++;
      }
      gsl_vector_int_set(nallele, lociCtr, wordCtr);
      if (wordCtr > max){
	max = wordCtr;
      }
    }
  }
  infile.close();
  return(max);
}

/* Get sequence read data */
void getreads(string readfile, datacont * data){
  int i = -1;
  int n = 0, k = 0;
  string line, oneword;
  ifstream infile;

  infile.open(readfile.c_str());
  if (!infile){
    cerr << "Cannot open file " << readfile << endl;
    exit(1);
  }

  // read in data 
  while (getline(infile, line)){ // read a line of input into line
    if (MAR == line[0]){ // MAR is the character designating a locus
      i++; // increment locus
      if (data->error >= 1){ // locus-specific error information is
	// included in this field, which follows
	// locus number
	istringstream stream(line);
	stream >> oneword;
	stream >> oneword; // discard "locus" and locus number, and position
	stream >> oneword;
	stream >> oneword;
	gsl_vector_set(data->errorvector,i,atof(oneword.c_str())); 
      }
      else { // set all errors probs. equal to data->error
	gsl_vector_set(data->errorvector,i,data->error); 
      }	
      n = 0;
    }
    else { // this is a line with count data
      istringstream stream(line);
      stream >> oneword; // read a word at a time
      k = atoi(oneword.c_str());
      gsl_matrix_int_set(data->allelecount, i, n, k); // count data 
      stream >> oneword; // read a word at a time
      k=k+atoi(oneword.c_str());
      gsl_matrix_int_set(data->nreads, i, n, k);
      n++;
    }
  }
  infile.close();
}
