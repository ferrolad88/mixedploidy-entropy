#!/usr/bin/python3

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

gmse=pd.read_csv("gmse.txt",sep=" ", header=None) 
gmse=np.resize(np.transpose(gmse),(4,4,3)) 

f,(ax1,ax2,ax3)= plt.subplots(1,3,gridspec_kw={'width_ratios':[1,1,1]})

ax1.get_shared_y_axes().join(ax2,ax3)

log_norm=LogNorm(vmin=gmse.min(), vmax=gmse.max())

sns.heatmap(data=gmse[:,:,0], ax=ax1, square=True, xticklabels=['1x', '2x', '4x', '6x'], yticklabels=['0.05', '0.1', '0.2', '0.4'], cbar=False, annot=True, norm=log_norm)
ax1.set_title('1 pop')
sns.heatmap(data=gmse[:,:,1], ax=ax2, square=True, xticklabels=['1x', '2x', '4x', '6x'], yticklabels=['0.05', '0.1', '0.2', '0.4'], cbar=False, annot=True, norm=log_norm)
ax2.set_title('2 pops')
sns.heatmap(data=gmse[:,:,2], ax=ax3, square=True, xticklabels=['1x', '2x', '4x', '6x'], yticklabels=['0.05', '0.1', '0.2', '0.4'], cbar=False, annot=True, norm=log_norm)
ax3.set_title('3 pops')

f.suptitle('genotype rmse')
plt.show()


files=['qmsepar.txt', 'qmsebc.txt', 'qmsef1.txt', 'qmsef2.txt', 'qmsef3.txt']

for temp in files:
	tempqmse=pd.read_csv(temp, sep=" ", header=None)
	tempqmse=np.resize(np.transpose(tempqmse), (4,4,2))

	f,(ax1,ax2)= plt.subplots(1,2,gridspec_kw={'width_ratios':[1,1]}, sharey=True)

	log_norm=LogNorm(vmin=tempqmse[:,:,0].min(), vmax=tempqmse[:,:,1].max())

	sns.heatmap(data=tempqmse[:,:,0], ax=ax1, square=True, xticklabels=['1x', '2x', '4x', '6x'], yticklabels=['0.05', '0.1', '0.2', '0.4'], cbar=False, annot=True, norm=log_norm)
	ax1.set_title('2 pops')
	sns.heatmap(data=tempqmse[:,:,1], ax=ax2, square=True, xticklabels=['1x', '2x', '4x', '6x'], yticklabels=['0.05', '0.1', '0.2', '0.4'], cbar=False, annot=True)
	ax2.set_title('3 pops')

	f.suptitle(temp)
	plt.show()

## for correlation with missing data
gcm=pd.read_csv("missinggeno/gcormiss.txt", sep=" ", header=None)
gcm=np.resize(np.transpose(gcm),(3,4,3)) 

f,(ax1,ax2,ax3)= plt.subplots(1,3,gridspec_kw={'width_ratios':[1,1,1]})

ax1.get_shared_y_axes().join(ax2,ax3)
sns.heatmap(data=gcm[:,:,0], ax=ax1, square=True, xticklabels=['10%', '20%', '30%', '40%'], yticklabels=['0.05', '0.1', '0.2'], cbar=False, annot=True, cmap=plt.get_cmap('viridis'))
ax1.set_title('1 pop')
sns.heatmap(data=gcm[:,:,1], ax=ax2, square=True, xticklabels=['10%', '20%', '30%', '40%'], yticklabels=['0.05', '0.1', '0.2'], cbar=False, annot=True, cmap=plt.get_cmap('viridis'))
ax2.set_title('2 pops')
sns.heatmap(data=gcm[:,:,2], ax=ax3, square=True, xticklabels=['10%', '20%', '30%', '40%'], yticklabels=['0.05', '0.1', '0.2'], cbar=False, annot=True, cmap=plt.get_cmap('viridis'))
ax3.set_title('3 pops')

f.suptitle('genotype correlation with missing data')
plt.show()

# for correlation for admix props when we have missing data
gcm=pd.read_csv("missinggeno/qcormiss.txt", sep=" ", header=None)
gcm=np.resize(np.transpose(gcm),(3,4,2))

f,(ax1,ax2)= plt.subplots(1,2,gridspec_kw={'width_ratios':[1,1]})

ax1.get_shared_y_axes().join(ax2)
sns.heatmap(data=gcm[:,:,0], ax=ax1, square=True, xticklabels=['10%', '20%', '30%', '40%'], yticklabels=['0.05', '0.1', '0.2'], cbar=False, annot=True, cmap=plt.get_cmap('viridis'))
ax1.set_title('2 pops')
sns.heatmap(data=gcm[:,:,1], ax=ax2, square=True, xticklabels=['10%', '20%', '30%', '40%'], yticklabels=['0.05', '0.1', '0.2'], cbar=False, annot=True, cmap=plt.get_cmap('viridis'))
ax2.set_title('3 pops')

f.suptitle('admixture proportion correlation with missing data')
plt.show()
