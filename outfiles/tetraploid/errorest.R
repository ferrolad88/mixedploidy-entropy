## parameters to be changed
library(rhdf5)
ploidy<-4

k<-1:3
fst<-1:4
cov<-c(1, 2, 4, 6, 12)

nind<-100
nloci<-2000

## finding error estimates for params by simple mse calculations for each ind and locus
gmse<-array(0, c(3,4,5))
qmse.par<-array(0, c(2,4,5))
qmse.f1<-array(0, c(2,4,5))
qmse.f2<-array(0, c(2,4,5))
qmse.bc<-array(0, c(2,4,5))
qmse.f3<-array(0, c(2,4,5))
qtot<-array(0, c(2,4,4))
for(m in 1:length(k)){
	if(k[m]==2){
		qfile<-paste0("../estfiles/qk",k[m],"true.txt")
		qtrue<-scan(qfile)
	}
	for(n in 1:length(fst)){
		genofile<-paste0("../../simfiles/tetraploid/g.k",k[m],".fst",fst[n],".txt")
		gtrue<-scan(genofile)
		for(o in 1:length(cov)){
			h5file<-paste0("outgl",k[m],".fst",fst[n],".",cov[o],"x.hdf5")
			# genotypes
			gp<-h5read(h5file, "gprob")
			gp<-apply(gp,c(2,3),function(x){sum(x*(0:ploidy))})
			gmse[m, n, o]<-sqrt(mean(abs(as.numeric(gp)-gtrue))^2)
			if(k[m]!=1){
				# admixture props
				w.q<-h5read(h5file, "q")
				q<-apply(w.q, c(2,3), mean)
				if(k[m]==2){
					qmse.par[m-1,n,o]<-sqrt(min(mean(abs(as.numeric(q[,1:50])-qtrue[1:100]))^2,mean(abs(as.numeric(q[,1:50])-(1-qtrue[1:100])))^2))
					qmse.f1[m-1,n,o]<-sqrt(min(mean(abs(as.numeric(q[,51:60])-qtrue[101:120]))^2,mean(abs(as.numeric(q[,51:60])-(1-qtrue[101:120])))^2))
					qmse.f2[m-1,n,o]<-sqrt(min(mean(abs(as.numeric(q[,61:70])-qtrue[121:140]))^2,mean(abs(as.numeric(q[,61:70])-(1-qtrue[121:140])))^2))
					qmse.bc[m-1,n,o]<-sqrt(min(mean(abs(as.numeric(q[,71:90])-qtrue[141:180]))^2,mean(abs(as.numeric(q[,71:90])-(1-qtrue[141:180])))^2))
					qmse.f3[m-1,n,o]<-sqrt(min(mean(abs(as.numeric(q[,91:100])-qtrue[181:200]))^2,mean(abs(as.numeric(q[,91:100])-(1-qtrue[181:200])))^2))
				}
				else if(k[m]==3){
					maxidx<-which.max(q[,1])
					if(which.max(q[,16])<which.max(q[,31])){
						qfile<-paste0("../estfiles/qk3true",maxidx,"a.txt")
						qtrue<-scan(qfile)
					}
					else{
						qfile<-paste0("../estfiles/qk3true",maxidx,"b.txt")
						qtrue<-scan(qfile)
					}
					qmse.par[m-1,n,o]<-sqrt(mean(abs(as.numeric(q[,1:45])-qtrue[1:135]))^2)
					qmse.f1[m-1,n,o]<-sqrt(mean(abs(as.numeric(q[,46:60])-qtrue[136:180]))^2)
					qmse.f2[m-1,n,o]<-sqrt(mean(abs(as.numeric(q[,61:75])-qtrue[181:225]))^2)
					qmse.bc[m-1,n,o]<-sqrt(mean(abs(as.numeric(q[,76:90])-qtrue[226:270]))^2)
					qmse.f3[m-1,n,o]<-sqrt(mean(abs(as.numeric(q[,91:100])-qtrue[271:300]))^2)
				}
			}
		}
	}
}

write.table(gmse, file="gmse.txt", qmethod = "double", row.names=F, col.names=F)
write.table(qmse.par, file="qmsepar.txt", qmethod = "double", row.names=F, col.names=F)
write.table(qmse.f1, file="qmsef1.txt", qmethod = "double", row.names=F, col.names=F)
write.table(qmse.f2, file="qmsef2.txt", qmethod = "double", row.names=F, col.names=F)
write.table(qmse.bc, file="qmsebc.txt", qmethod = "double", row.names=F, col.names=F)
write.table(qmse.f3, file="qmsef3.txt", qmethod = "double", row.names=F, col.names=F)

