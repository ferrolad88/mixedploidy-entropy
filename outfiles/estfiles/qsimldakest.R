nind<-100
nloci<-2000
print("creating matrix")
g<-t(matrix(scan("simfiles/hexaploid/g.k3.fst4.txt",n=nind*nloci),nrow=nind,ncol=nloci))

do.pca<-function(gmat, write.gcov=FALSE, inds=""){
	print("centering genotype matrix")
    gmn<-apply(gmat,1,mean, na.rm=T)
    gmnmat<-matrix(gmn,nrow=nrow(gmat),ncol=ncol(gmat))
    gprime<-gmat-gmnmat ## remove mean
    
	print("finding pairwise covariance")
    gcovarmat<-matrix(NA,nrow=ncol(gmat),ncol=ncol(gmat))
    for(i in 1:ncol(gmat)){
        for(j in i:ncol(gmat)){
            if (i==j){
                gcovarmat[i,j]<-cov(gprime[,i],gprime[,j], use="pairwise.complete.obs")
            }
            else{
                gcovarmat[i,j]<-cov(gprime[,i],gprime[,j], use="pairwise.complete.obs")
                gcovarmat[j,i]<-gcovarmat[i,j]
            }
        }
    }
    if(write.gcov==TRUE){
        inds<-ifelse(inds == "", paste("i", 1:ncol(gmat)), inds)
        write.table(round(gcovarmat,5),file="gcovarmat.txt",
                    quote=F,row.names=F,col.names=inds)
    }
	print("final principal comp analysis")
    prcomp(x=gcovarmat,center=TRUE,scale=FALSE)
}
print("covariate matrix calc")
pcout<-do.pca(g)
pcSummary<-summary(pcout)
print("k-means, lda and writing to file")
library(MASS)
#k2<-kmeans(pcout$x[,1:5],2,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak2<-lda(x=pcout$x[,1:5], grouping=k2$cluster,CV=TRUE)
#write.table(round(ldak2$posterior,4),quote=F,row.names=F,col.names=F,file="simfiles/hexaploid/qk3fst4.txt")

k3<-kmeans(pcout$x[,1:5],3,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
ldak3<-lda(x=pcout$x[,1:5], grouping=k3$cluster,CV=TRUE)
write.table(round(ldak3$posterior,5),quote=F,row.names=F,col.names=F,file="simfiles/hexaploid/qk3fst4.txt")

